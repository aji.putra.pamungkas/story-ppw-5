from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('', views.matkul, name='matkul'),
    path('tambah/', views.tambah, name='tambah'),
    path('delete/', views.delete, name='delete'),
    path('deleteall/', views.deleteall, name='deleteall')
]