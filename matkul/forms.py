from django import forms
from .models import Matkul

class input_data(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama_matkul','dosen','sks','deskripsi','semester']
    error_messages = {
		'required' : 'Please Type'
	}
    input_attrs1 = {
		'type' : 'text',
		'placeholder' : 'Nama Matkul'
	}
    input_attrs2 = {
		'type' : 'text',
		'placeholder' : 'Nama Dosen'
	}
    input_attrs3 = {
		'type' : 'text',
		'placeholder' : 'Jumlah SKS'
	}
    input_attrs4 = {
		'type' : 'text',
		'placeholder' : 'Deskripsi Matkul'
	}
    input_attrs5 = {
		'type' : 'text',
		'placeholder' : 'Semester'
	}
    nama_matkul = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs1))
    dosen = forms.CharField(label='', max_length=100, widget=forms.TextInput(attrs=input_attrs2))
    sks = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs=input_attrs3))
    deskripsi = forms.CharField(label='', max_length=255, widget=forms.TextInput(attrs=input_attrs4))
    semester = forms.CharField(label='', max_length=30, widget=forms.TextInput(attrs=input_attrs5))

class input_delete(forms.Form):
    input_attrs = {
		'type' : 'text',
		'placeholder' : 'Nama Matkul'
	}
    nama_matkul = forms.CharField(label='', max_length=200, widget=forms.TextInput(attrs=input_attrs))